$(document).ready(function () {
    $('.form-select2').select2({
        width: '100%'
    });
    let msg = $('.session').data('session');
    let type = $('.session').data('type');
    if (msg, type) {
        $.notify({
            icon: "add_alert",
            message: msg
        }, {
            type: type,
            timer: 3000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
    }
    CKEDITOR.replace('content',
        {
            height: '400px',
        });
    CKEDITOR.replace('question_content',
        {
            height: '200px',
        });

    $('#test-table').dataTable({
        'pagingType': 'first_last_numbers',
        'searching': 'true',
        "ordering": false,
    });

    $('#datatables').dataTable({
        'pagingType': 'first_last_numbers',
        'searching': 'true',
    });

    $('#chapter-table').dataTable({
        'pagingType': 'first_last_numbers',
        'searching': 'true',
        "info": false,
        "ordering": false
    });
    $('#notification-table').dataTable({
        'pagingType': 'first_last_numbers',
        'searching': 'true',
        "columnDefs": [
            {
                "targets": [4],
                "orderable": false,
            },
        ],
    });
});
