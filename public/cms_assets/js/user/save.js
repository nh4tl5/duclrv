$(document).ready(function () {
    $("input[name='role']").on('change', function () {
        let role = $("input[name='role']:checked").val();
        console.log(role === 'user')
        if (role === 'user') {
            $('.row_user_permission').removeClass('d-none');
        } else {
            $('.row_user_permission').addClass('d-none');
        }
    });
});
