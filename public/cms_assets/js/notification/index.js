$(document).ready(function() {
    var datatable = $('#notification-table').dataTable({
        'pagingType': 'first_last_numbers',
        'searching': 'true',
    });

    $('.btn_delete_item').click(function () {
        $('.btn_confirm_delete_item').attr('href', $(this).data('url'));
    });
});
