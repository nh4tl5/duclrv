$(document).ready(function() {
    var datatable = $('#document-table').dataTable({
        'pagingType': 'first_last_numbers',
        'searching': 'true',
    });

    $('#thumbnail').change(function() {
        const file = this.files[0];
        const fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        if (file) {
            $('#image-preview').empty();
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) !== -1) {
                let reader = new FileReader();
                reader.onload = function (event) {
                  $('#image-preview').append(`<div class="card col-sm-7" id="image-preview"><img class="card-img-top" src="${event.target.result}" alt="Card image cap"></div>`)
                }
                reader.readAsDataURL(file);
            }
        }
    });

    $('#remove-thumbnail').click(function() {
        $('#image-preview').empty();
    })

    $(document).on('click', '#btn-delete', function () {
        $('#confirm-delete').attr("href", $(this).attr("data-href"))
    })
});
