$(document).ready(function() {
    var datatable = $('#test-detail-table').dataTable({
        'pagingType': 'first_last_numbers',
        'searching': 'true',
    });

    $(document).on('click', '#btn-delete', function () {
        $('#confirm-delete').attr("href", $(this).attr("data-href"))
    })
});
