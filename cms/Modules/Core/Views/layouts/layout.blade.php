<!doctype html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        @if(View::hasSection('title'))
            @yield('title')
        @else
            Coaching School
        @endif
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta property="fb:app_id" content="ー">
    <meta property="og:url" content="">
    <meta property="og:type" content="article">
    <meta property="og:title" content="">
    <meta property="og:locale" content="ja_JP">
    <meta property="og:image" content="">
    <meta property="og:description" content="">
    <meta property="og:site_name" content="">
    <meta property="Twitter:card" content="summary_large_image">
    <meta property="Twitter:site" content="@dmmolg_mash">
    <meta property="Twitter:creator" content="@dmmolg_mash">
    <meta property="Twitter:title" content="">
    <meta property="Twitter:description" content="">
    <meta property="Twitter:image" content="">
    <meta property="Twitter:url" content="">
    <link rel="icon" type="image/x-icon" href="{{ cxl_asset('assets_client/images/favicon.ico') }}">
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ cxl_asset('assets_client/images/favicon.ico') }}">
    <link href="{{ cxl_asset('assets_client/libs/css/jquery.datetimepicker.min.css')}}" rel="stylesheet" type='text/css'>
    <link href="{{ cxl_asset('assets_client/css/style.css') }}" rel="stylesheet" type='text/css'>
    <link rel="stylesheet" href="{{ cxl_asset('assets_client/css/config/_reset.scss') }}">
    @yield('css')
    @stack('style')
</head>
<body class="@yield('body-class')" id="@yield('body-id')">

    <!-- navigation -->
    @yield('navbar')
    <!-- navigation -->

    <!-- Header -->
    <header id="header" class="@yield('header-class')">
        @yield('header')
    </header>

    <!-- /header -->
    <main id="main">
        <!-- Content -->
        @yield('content')
        <!-- /Content -->
        @include('Core::layouts.flash_message')
    </main>

    <!-- aside -->
    @yield('aside')
    <!-- /aside -->

    <!-- Modal Popup -->
    @yield('modal')
    <!-- /Modal Popup -->

    <!-- footer -->
    @yield('footer')
    <!-- /footer -->

    <script defer type="text/javascript" src="{{ cxl_asset('assets_client/libs/js/jquery-3.6.0.min.js') }}"></script>
    <script defer type="text/javascript" src="{{ cxl_asset('assets_client/libs/js/jquery.datetimepicker.full.min.js')}}"></script>
    <script defer type="text/javascript" src="{{ cxl_asset('assets_client/libs/js/jquery.validate.min.js') }}"></script>
    <script defer type="text/javascript" src="{{ cxl_asset('assets_client/js/main.js') }}"></script>
    @stack('js')
</body>

</html>
