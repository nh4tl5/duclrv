@if(Session::has('msg'))
<div class="session" data-type="{{ Session::has('type') ? Session::get('type') : 'success' }}" data-session="{{ Session::has('msg') ? Session::get('msg') : ''}}"></div>
@endif

@if(Session::has('message'))
<div class="session" data-type="{{ Session::has('type') ? Session::get('type') : 'success' }}" data-session="{{ Session::has('message') ? Session::get('message') : ''}}"></div>
@endif

@if(Session::has('success'))
<div class="session" data-type="{{ Session::has('type') ? Session::get('type') : 'success' }}" data-session="{{ Session::has('success') ? Session::get('success') : ''}}"></div>
@endif

<script type="text/javascript">
    if (performance.navigation.type == 2) {
        location.reload();
    }
</script>
