@if ($message = Session::get('success'))
<div class="alert-popup">
    <div class="layout"></div>
    <div class="alert-content">
        <span class="alert-text"> {{ $message }}! </span>
        <button type="button" class="close">
            <span>近い</span>
        </button>
    </div>
</div>
@else
<div class="alert-popup" style="display: none;">
    <div class="layout"></div>
    <div class="alert-content">
        <span class="alert-text"> このページを表示するには、ログインする必要があります! </span>
        <button type="button" class="close">
            <a href="{{ route('login') }}">ログインする</a>
        </button>
    </div>
</div>
@endif
