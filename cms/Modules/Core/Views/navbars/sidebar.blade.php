<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ asset('assets/img/sidebar-1.jpg') }}">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
      <a href="/" class="simple-text logo-normal text-center">
          SHOP
      </a>
  </div>
  <div class="sidebar-wrapper">
      <ul class="nav">
          <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
              <a class="nav-link" href="/">
                  <i class="material-icons">dashboard</i>
                  <p>{{ __('DashBroad') }}</p>
              </a>
          </li>
          <li class="nav-item{{ $activePage == 'user' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('cms.user.index') }}">
                  <i class="material-icons">account_circle</i>
                  <p>{{ __('User') }}</p>
              </a>
          </li>
{{--          <li class="nav-item{{ $activePage == 'course' ? ' active' : '' }}">--}}
{{--              <a class="nav-link" href="{{ route('cms.course.index') }}">--}}
{{--                  <i class="material-icons">library_books</i>--}}
{{--                  <p>{{ __('教材登録管理章') }}</p>--}}
{{--              </a>--}}
{{--          </li>--}}
{{--          <li class="nav-item{{ $activePage == 'notification' ? ' active' : '' }}">--}}
{{--              <a class="nav-link" href="{{ route('cms.notification.index') }}">--}}
{{--                  <i class="material-icons">notifications</i>--}}
{{--                  <p>{{ __('お知らせ管理') }}</p>--}}
{{--              </a>--}}
{{--          </li>--}}
{{--          <li class="nav-item{{ $activePage == 'document' ? ' active' : '' }}">--}}
{{--              <a class="nav-link" href="{{ route('admin.document.list') }}">--}}
{{--                  <i class="material-icons">file_present</i>--}}
{{--                  <p>{{ __('DL素材管理') }}</p>--}}
{{--              </a>--}}
{{--          </li>--}}
{{--          <li class="nav-item{{ $activePage == 'final-test' ? ' active' : '' }}">--}}
{{--              <a class="nav-link" href="{{ route('cms.test.finalTest') }}">--}}
{{--                  <i class="material-icons">quiz</i>--}}
{{--                  <p>{{ __('筆記試験定試管理') }}</p>--}}
{{--              </a>--}}
{{--          </li>--}}
      </ul>
  </div>
</div>
