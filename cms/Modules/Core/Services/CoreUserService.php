<?php

namespace Cms\Modules\Core\Services;

use Cms\Modules\Core\Repositories\Contracts\CoreUserRepositoryContract;
use Illuminate\Support\Facades\Hash;

class CoreUserService
{
    protected $user;

    public function __construct(CoreUserRepositoryContract $user)
    {
        $this->user = $user;
    }

    public function getAll()
    {
        return $this->user->getAll();
    }

    public function store($data)
    {
        $data['password'] = Hash::make($data['password']);

        return $this->user->store($data);
    }

    public function find($id)
    {
        return $this->user->find($id);
    }

    public function update($id, $data)
    {
        return $this->user->update($id, $data);
    }

    public function delete($id)
    {
        if ($this->user->find($id)) {
            return $this->user->delete($id);
        }
    }
}
