<?php

namespace Cms\Modules\User\Repositories;

use Cms\Modules\User\Repositories\Contracts\UserRepositoryContract;
use Cms\Modules\Core\Repositories\CoreUserRepository;

class UserRepository extends CoreUserRepository implements UserRepositoryContract
{
    public function getAllPermissionByUser()
    {
        return $this->model->with(['permissions' => function ($queryPermission) {
            $queryPermission->orderby('priority', 'desc');
        }])->get();
    }
}
