<?php

namespace Cms\Modules\User\Repositories\Contracts;

use Cms\Modules\Core\Repositories\Contracts\CoreUserRepositoryContract;

interface UserRepositoryContract extends CoreUserRepositoryContract
{
    public function getAllPermissionByUser();
}
