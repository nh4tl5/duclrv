<?php

namespace Cms\Modules\User\Services\Contracts;

use Cms\Modules\Core\Services\Contracts\CoreUserServiceContract;

interface UserServiceContract extends CoreUserServiceContract
{
    public function cmsStore($data);

    public function cmsUpdate($id, $data);

    public function getAllPermissionByUser();
}
