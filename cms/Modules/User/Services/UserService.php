<?php

namespace Cms\Modules\User\Services;

use Cms\Modules\User\Services\Contracts\UserServiceContract;
use Cms\Modules\Core\Services\CoreUserService;
use Cms\Modules\User\Repositories\Contracts\UserRepositoryContract;
use Illuminate\Support\Facades\Hash;

class UserService extends CoreUserService implements UserServiceContract
{
	protected $repository;

	public function __construct(UserRepositoryContract $repository)
	{
	    parent::__construct($repository);

	    $this->repository = $repository;
	}

	public function cmsStore($data)
	{   $data['password'] = Hash::make($data['password']);
		$user = $this->repository->store($data);
		$user->assignRole($data['role']);
		$userPermissions = [];
		$user->givePermissionTo($userPermissions);

		return $user;
	}

	public function cmsUpdate($id, $data)
	{
		$user = $this->repository->find($id);
		$user->update($data);
		$user->syncRoles($data['role']);


		return $user;
	}

    public function getAllPermissionByUser()
    {
        return $this->repository->getAllPermissionByUser();
    }

}
