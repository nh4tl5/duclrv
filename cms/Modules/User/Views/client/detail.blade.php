@extends('Core::layouts.layout')
@section('header-class', 'header active')
@section('navbar')
<!-- navigation -->
@include('Core::navbars.client_sidebar', ['activePage' => 'user'])
<!-- navigation -->
@endsection
@section('header')
@include('Core::headers.header')
@endsection
@section('body-class', 'page_student')
@section('content')
<div class="inner">
    <section class="section" id="my_page">
        <div class="inner">
            <div class="content">
                <h2>マイページ > 受講者</h2>
                <h3 class="tlt_section">お知らせ</h3>
                <div class="list_document">
                    <ul>
                        @foreach($courseByUser as $course)
                        <li>
                            <a href="">
                                <div class="tlt">
                                    <h4>{{ $course->name }}</h4>
                                    <h5>日付</h5>
                                </div>
                                <p>{{ $course->description }}</p>
                                <h6>詳しく見る</h6>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="section" id="info_member">
        <div class="inner">
            <div class="content">
                <h3 class="tlt_section">会員情報</h3>
                @include('Core::layouts.flash_message')
                <form method="POST" action="{{ route('client.user.update', ['id' => $user->id]) }}" id="update_user_information" disabled='disable'>
                    @csrf
                    <h4>受講生番号 : <span>000AA</span></h4>
                    <div class="item_input">
                        <div class="tlt_item">
                            <h3>氏名</h3>
                        </div>
                        <div class="item_field">
                            <input class="input" type="text" name="name" id="name" placeholder="氏名" value="{{ $user->name }}">
                        </div>
                    </div>
                    <div class="item_input list_radio">
                        <div class="tlt_item">
                            <h3>性別</h3>
                        </div>
                        <div class="item_radio">
                            <label for="male">
                                <input class="input" type="radio" name="gender" value="male" id="male" {{ $user->gender == 'male' ? 'checked' : '' }}>
                                <span>男性</span>
                            </label>
                            <label for="female">
                                <input class="input" type="radio" name="gender" value="female" id="female" {{ $user->gender == 'female' ? 'checked' : '' }}>
                                <span>女性</span>
                            </label>
                        </div>
                    </div>
                    <div class="item_input birthday">
                        <div class="tlt_item">
                            <h3>年齢</h3>
                        </div>
                        <div class="item_field">
                            <input class="input" type="text" readonly name="birthday" id="birthday" placeholder="00/00/0000" value="{{ date('d/m/Y', strtotime($user->date_of_birth)) }}">
                            <span></span>
                        </div>
                    </div>
                    <div class="item_input">
                        <div class="tlt_item">
                            <h3>お住いの都道府県</h3>
                        </div>
                        <div class="item_field">
                            <input class="input" type="text" name="address" id="address" placeholder="福岡" value="{{ $user->address }}">
                        </div>
                    </div>
                    <div class="item_input">
                        <div class="tlt_item">
                            <h3>メールアドレス</h3>
                        </div>
                        <div class="item_field">
                            <input class="input" type="email" name="email" id="email" placeholder="Coachingschool.com" value="{{ $user->email }}">
                        </div>
                    </div>
                    <div class="choice">
                        <div class="btn_outline btn_large">
                            <a href="javascript:void(0)" id="enable_user_information">編集</a>
                        </div>
                        <div class="btn btn_large">
                            <a href="javascript:void(0)" id="confirm_user_information">確定</a>
                        </div>
                    </div>
                    <div class="progress">
                        <h5>学習の進捗</h5>
                        <div class="progress_line">
                            <div class="detail_progress"></div>
                        </div>
                    </div>
                </form>
                <div class="info_student">
                    <div class="thumb">
                        <img src="{{ cxl_asset('assets_client/images/certificate.png') }}" alt="">
                    </div>
                    <a class="btn_download" href="">ダウンロード</a>
                    <h4>試験日時 : 00/00/000 - 00:00時</h4>
                    <div class="btn btn_large">
                        <a href="#">試験</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@push('js')
<script defer type="text/javascript" src="{{ cxl_asset('assets_client/libs/js/jquery-3.6.0.min.js') }}"></script>
<script defer type="text/javascript" src="{{ cxl_asset('assets_client/libs/js/jquery.datetimepicker.full.min.js') }}"></script>
<script defer type="text/javascript" src="{{ cxl_asset('assets_client/js/main.js') }}"></script>
<script defer type="text/javascript" src="{{ cxl_asset('assets_client/js/detail_user.js') }}"></script>
@endpush
