@extends('Core::layouts.layout')
@section('header-class', 'header active')
@section('navbar')
    <!-- navigation -->
    @include('Core::navbars.client_sidebar', ['activePage' => 'student'])
    <!-- navigation -->
@endsection
@section('header')
    @include('Core::headers.header')
@endsection
@section('body-class', 'page_student')
@section('content')
    <div class="inner">
        <section class="section" id="my_page">
            <div class="inner">
                <h2>マイページ > 受講者</h2>
                <h3 class="tlt_section">お知らせ</h3>
                <div class="list_document">
                    <ul>
                        <li>
                            <a href="">
                                <div class="tlt">
                                    <h4>お知らせ見出し</h4>
                                    <h5>日付</h5>
                                </div>
                                <p>お知らせ本文 テキステキステキステキステキステキステキステキステキステキステキステキステキステキ テキステキステキステキステキステキステキステキステキステキステキステキステキステキステキステキ テキステキステキステキステキステキステキステキステキステキステキステキステキステキステキステキ ステキステキステキステキステキステキステキステキステキステキス。</p>
                                <h6>詳しく見る</h6>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div class="tlt">
                                    <h4>お知らせ見出し</h4>
                                    <h5>日付</h5>
                                </div>
                                <p>お知らせ本文 テキステキステキステキステキステキステキステキステキステキステキステキステキステキ テキステキステキステキステキステキステキステキステキステキステキステキステキステキステキステキ テキステキステキステキステキステキステキステキステキステキステキステキステキステキステキステキ ステキステキステキステキステキステキステキステキステキステキス。</p>
                                <h6>詳しく見る</h6>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="section" id="info_member">
            <div class="inner">
                <h3 class="tlt_section">会員情報</h3>
                <form action="">
                    <h4>受講生番号 : <span>000AA</span></h4>
                    <div class="item_input">
                        <div class="tlt_item">
                            <h3>氏名</h3>
                        </div>
                        <div class="item_field">
                            <input class="input" type="text" name="fullname" id="fullname" placeholder="氏名">
                        </div>
                    </div>
                    <div class="item_input list_radio">
                        <div class="tlt_item">
                            <h3>性別</h3>
                        </div>
                        <div class="item_radio">
                            <label for="male">
                                <input class="input" type="radio" name="sex" id="male">
                                <span>男性</span>
                            </label>
                            <label for="female">
                                <input class="input" type="radio" name="sex" id="female">
                                <span>女性</span>
                            </label>
                        </div>
                    </div>
                    <div class="item_input birthday">
                        <div class="tlt_item">
                            <h3>年齢</h3>
                        </div>
                        <div class="item_field">
                            <input class="input" type="text" readonly name="birthday" id="birthday" placeholder="00/00/0000">
                            <span></span>
                        </div>
                    </div>
                    <div class="item_input">
                        <div class="tlt_item">
                            <h3>お住いの都道府県</h3>
                        </div>
                        <div class="item_field">
                            <input class="input" type="text" name="country" id="country" placeholder="福岡">
                        </div>
                    </div>
                    <div class="item_input">
                        <div class="tlt_item">
                            <h3>メールアドレス</h3>
                        </div>
                        <div class="item_field">
                            <input class="input" type="email" name="email" id="email" placeholder="Coachingschool.com">
                        </div>
                    </div>
                    <div class="choice">
                        <div class="btn_outline btn_large">
                            <a href="#">編集</a>
                        </div>
                        <div class="btn btn_large">
                            <a href="#">確定</a>
                        </div>
                    </div>
                    <div class="progress">
                        <h5>学習の進捗</h5>
                        <div class="progress_line">
                            <div class="detail_progress"></div>
                        </div>
                    </div>
                </form>
                <div class="info_student">
                    <div class="thumb">
                        <img src="./assets/images/certificate.png" alt="" >
                    </div>
                    <a class="btn_download" href="">ダウンロード</a>
                    <h4>試験日時 : 00/00/000 - 00:00時</h4>
                    <div class="btn btn_large">
                        <a href="#">試験</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('js')
@endpush
