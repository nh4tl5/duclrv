@extends('Core::layouts.layout_cms', ['activePage' => 'user', 'titlePage' => __('')])
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary card-header-icon">
                        <h4 class="card-icon">{{ __('ユーザ詳細') }}</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('ユーザ名') }}</label>
                            <div class="col-sm-7 d-flex align-items-center">
                                <span>{{ $user->name }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('メールアドレス') }}</label>
                            <div class="col-sm-7 d-flex align-items-center">
                                <span>{{ $user->email }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('性別') }}</label>
                            <div class="col-sm-7 d-flex align-items-center">
                                <span>{{ $user->gender }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('生年月日') }}</label>
                            <div class="col-sm-7 d-flex align-items-center">
                                <span>{{ $user->date_of_birth }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label" for="input-addess">{{ __('お住いの都道府県') }}</label>
                            <div class="col-sm-7 d-flex align-items-center">
                                <span>{{ $user->address }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card course_user_test mt-5">
                    <div class="card-header card-header-primary card-header-icon">
                        <h4 class="card-icon">{{ __('登録コース') }}</h4>
                    </div>
                    <div class="course p-3">
                        @foreach($courseByUser as $course)
                        <div class="card">
                            <div class="btn btn-link collapsed" data-toggle="collapse" data-target="#course-{{ $course->id }}" aria-controls="course-{{ $course->id }}">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('コース名') }}</label>
                                    <div class="col-sm-7 d-flex align-items-center">
                                        <span>{{ $course->name }}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('状態') }}</label>
                                    <div class="col-sm-7 d-flex align-items-center">
                                        <span>{{ $course->status }}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('開始日') }}</label>
                                    <div class="col-sm-7 d-flex align-items-center">
                                        <span>{{ $course->created_at }}</span>
                                    </div>
                                </div>
                            </div>
                            <div id="course-{{ $course->id }}" class="collapse">
                                @if($course)
                                @foreach($course->test as $test)
                                @if($test->type == 'chapter')
                                <div class="card-body mini_test">
                                    <h4 class="text-center">{{ __('ミニテスト一覧') }}</h4>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">{{ __('ミニテスト名') }}</th>
                                                <th scope="col">{{ __('ポイント') }}</th>
                                                <th scope="col">{{ __('状態') }}</th>
                                                <th scope="col">{{ __('アクション') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="table-primary">
                                                <th scope="row">{{ $test->id }}</th>
                                                <td>{{ $test->title }}</td>
                                                @foreach($test->user as $item)
                                                <td>{{ $item->pivot->point ? $item->pivot->point : '' }}</td>
                                                @endforeach
                                                <td>{{ $test->status }}</td>
                                                <td>
                                                    <a href="{{ route('cms.user.test.edit', ['id' => $test->id]) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    <a href="{{ route('cms.user.test.detail', ['id' => $test->id]) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    <a href="{{ route('cms.user.test.delete', ['id' => $test->id]) }}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                @else
                                <div class="card-body final_test">
                                    <h4 class="text-center text-bold">{{ __('最終試験一覧') }}</h4>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">{{ __('最終テスト名') }}</th>
                                                <th scope="col">{{ __('ポイント') }}</th>
                                                <th scope="col">{{ __('状態') }}</th>
                                                <th scope="col">{{ __('アクション') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="table-success">
                                                <th scope="row">{{ $test->id }}</th>
                                                <td>{{ $test->title }}</td>
                                                @if ($test->user->toArray()==[])
                                                    <td></td>
                                                @else
                                                    @foreach($test->user as $item)
                                                    <td>{{ $item->pivot->point ? $item->pivot->point : '' }}</td>
                                                    @endforeach
                                                @endif
                                                <td>{{ $test->status }}</td>
                                                <td>
                                                    <a href="{{ route('cms.user.test.edit', ['id' => $test->id]) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    <a href="{{ route('cms.user.test.detail', ['id' => $test->id]) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    <a href="{{ route('cms.user.test.delete', ['id' => $test->id]) }}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                @endif
                                @endforeach
                                @else
                                <p class="text-center">No data</p>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
@endpush
