@extends('Core::layouts.layout_cms', ['activePage' => 'table', 'titlePage' => __('')])

@section('content')
<div class="content">
    <div class="container-fluid ">
        <div class="card">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <h4 class="card-title">{{ __('') }}</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('ユーザ名') }}</label>
                    <div class="col-sm-7">
                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" disabled name="name" id="input-name" type="text" placeholder="{{ __('ユーザ名を入力してください') }}" value="{{ $userTest->user->name }}" required="true" aria-required="true" />
                            @if ($errors->has('name'))
                            <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('メールアドレス') }}</label>
                    <div class="col-sm-7">
                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" disabled name="email" id="input-email" type="email" placeholder="{{ __('メールアドレスを入力してください') }}" value="{{ $userTest->user->email }}" required readonly />
                            @if ($errors->has('email'))
                            <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('性別') }}</label>
                    <div class="col-sm-7 d-flex align-items-end">
                        <div class="form-check form-check-radio form-check-inline mb-0">
                            <label class="form-check-label">
                                <input class="form-check-input" disabled {{ $userTest->user->gender == 'female' ? 'checked' : '' }} type="radio" name="gender" id="gender1" value="female">女性
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check form-check-radio form-check-inline mb-0">
                            <label class="form-check-label">
                                <input class="form-check-input" disabled {{ $userTest->user->gender == 'male' ? 'checked' : '' }} type="radio" name="gender" id="gender2" value="male">男性
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('生年月日') }}</label>
                    <div class="col-sm-7">
                        <div class="form-group{{ $errors->has('date_of_birth') ? ' has-danger' : '' }}">
                            <input class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}" name="date_of_birth" id="input-date-of-birth" type="date" placeholder="{{ __('生年月日を選択してください') }}" value="{{ $userTest->user->date_of_birth }}" disabled />
                            @if ($errors->has('date_of_birth'))
                            <span id="date-f-birth-error" class="error text-danger" for="input-date-of-birth">{{ $errors->first('date_of_birth') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('試験日') }}</label>
                    <div class="col-sm-7">
                        <div class="form-group">
                            <input class="form-control" name="date_of_test" type="text" disabled value="{{ date('H:i:s d/m/Y', strtotime($userTest->created_at)) }}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form class="form-horizontal form mx-auto w-75" method="POST" action="" enctype="multipart/form-data">
                    @csrf
                    <div class="card ">
                        <div class="card-body ">
                            <div class="form-group mt-5">
                                <label for="test_title" class="form-label text-dark">小テスト名</label>
                                <input type="text" name="title" value="{{ $userTest->test->title }}" class="form-control" id="test_title" placeholder="テスト名を入力してください" disabled>
                            </div>
                            <div class="form-group mt-5">
                                <label for="test_description" class="form-label text-dark">小テストデスクリプション</label>
                                @error('description')
                                <p class="error text-danger">{{ $message }}</p>
                                @enderror
                                <textarea class="form-control" name="description" id="test_description" rows="3" disabled>{{ $userTest->test->description }}</textarea>
                            </div>
                            <div class="form-group mt-5">
                                <label for="test_answer" class="form-label text-dark">答え</label>
                                <input type="text" name="answer" value="{{ $userTest->answer }}" class="form-control" id="test_answer" disabled>
                            </div>
                            <div class="form-group mt-5">
                                <label for="test_point" class="form-label text-dark">ポイント</label>
                                <input type="text" name="point" value="{{ $userTest->point }}" class="form-control" id="test_point" disabled>
                            </div>
                            <div class="form-group mt-5">
                                <label for="test_status" class="form-label text-dark">状況</label>
                                <input type="text" name="status" value="{{ $userTest->test->status }}" class="form-control" id="test_status" disabled>
                            </div>
                            <div class="form-group mt-5">
                                <label for="test_type" class="form-label text-dark">試験の種類</label>
                                <input type="text" name="type" value="{{ $userTest->test->type }}" class="form-control" id="test_type" disabled>
                            </div>
                            <div class="card-footer mt-5 mx-auto ml-auto">
                                <button type="submit" class="mx-auto btn btn-primary"><a href="{{ route('cms.user.detail', ['id' => $userTest->user_id]) }}" class="text-white">戻ってくる</a></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
