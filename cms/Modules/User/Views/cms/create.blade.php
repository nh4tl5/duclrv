@extends('Core::layouts.layout_cms', ['activePage' => 'user', 'titlePage' => __('Dashboard')])
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('cms.user.store') }}" class="form-horizontal">
                        @csrf
                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Create User') }}</h4>
{{--                                <p class="card-category">{{ __('ユーザ情報') }}</p>--}}
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Tên Tài Khoản') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="username..." value="{{ old('name') }}" required="true" aria-required="true"/>
                                            @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="email" value="{{ old('email') }}" required />
                                            @if ($errors->has('email'))
                                                <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Giới tính</label>
                                    <div class="col-sm-7 d-flex align-items-end">
                                        <div class="form-check form-check-radio form-check-inline  mb-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" {{ old('gender') == 'female' ? 'checked' : '' }} type="radio" name="gender" id="gender1" value="female">Nữ
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="form-check form-check-radio form-check-inline  mb-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" {{ old('gender') == 'male' ? 'checked' : '' }} type="radio" name="gender" id="gender2" value="male">Nam
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Vai Trò</label>
                                    <div class="col-sm-7 d-flex align-items-end">
                                        <div class="form-check form-check-radio form-check-inline  mb-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" {{ old('role') == 'admin' ? 'checked' : '' }} type="radio" name="role" id="role1" value="admin">Admin
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="form-check form-check-radio form-check-inline  mb-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" {{ old('role', 'user') == 'user' ? 'checked' : '' }} type="radio" name="role" id="role2" value="user">user
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Ngày Sinh</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('date_of_birth') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}" name="date_of_birth" id="input-date-of-birth" type="date" placeholder="{{ __('生年月日を選択してください') }}" value="{{ old('date_of_birth') }}"/>
                                            @if ($errors->has('date_of_birth'))
                                                <span id="date-f-birth-error" class="error text-danger" for="input-date-of-birth">{{ $errors->first('date_of_birth') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label" for="input-addess">Địa Chỉ</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" input type="text" name="address" id="input-addess" placeholder="address..." value="{{ old('address') }}" />
                                            @if ($errors->has('address'))
                                                <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('address') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label" for="input-current-password">Mật Khẩu</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('passowrd') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" input type="password" name="password" id="input-current-password" placeholder="password.." value="" required />
                                            @if ($errors->has('password'))
                                                <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('password') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label" for="input-password-confirmation">Xác nhận mật khẩu</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input class="form-control" name="password_confirmation" id="input-password-confirmation" type="password" placeholder="confirm password..." value="" required />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer ml-auto mr-auto">
                                <a class="btn btn-secondary mr-5" href="{{route('cms.user.index')}}">Cancel</a>
                                <button type="submit" class="btn btn-primary ml-5">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ cxl_asset('/cms_assets/js/user/save.js') }}"></script>
@endpush
