@extends('Core::layouts.layout_cms', ['activePage' => 'user', 'titlePage' => __('')])
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                    <div class="card-header card-header-primary card-header-icon">
                        <h4 class="card-icon "><i class="material-icons">account_circle</i></h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 text-right">
                                <a href="{{ route('cms.user.create') }}" class="btn btn-sm btn-primary">Create</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table" id="user-table">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>
                                            STT
                                        </th>
                                        <th>
                                            Tên
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Ngày Sinh
                                        </th>
                                        <th>
                                            Permission
                                        </th>
                                        <th>
                                            Giới Tính
                                        </th>
                                        <th>
                                            Địa Chỉ
                                        </th>
                                        <th class="text-right"></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($users as $user)
                                        <tr>
                                            <td>{{ $user->id}}</td>
                                            <td>{{ $user->name}}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->date_of_birh }}</td>
                                            <td>

                                            </td>
                                            <td>{{ $user->gender }}</td>
                                            <td>{{ $user->address }}</td>
                                            <td class="td-actions text-right">
                                                <div class="dropdown">
                                                    <span id="dropdownMenuButton" data-toggle="dropdown" type="button" class="material-icons">
                                                        list
                                                    </span>

                                                    <div class="ml-5 dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        <a class="dropdown-item" href="{{ route('cms.user.detail', ['id' => $user->id]) }}">Detail</a>
                                                        <a class="dropdown-item show-course" target="_blank" href="{{ route('cms.user.edit', ['id' => $user->id]) }}">Edit</a>
                                                        @if (Auth::id() != $user['id'])
                                                            <a class="dropdown-item btn-link btn_delete_item" data-url="{{ route('cms.user.delete', ['id' =>  $user->id]) }}" data-toggle="modal" data-target="#confirm_delete_item" href="javascript:void(0)">Delete</a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- modal confirm delete -->
    <div class="modal fade" id="confirm_delete_item">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-center">Bạn có chắc chắn muốn xóa người dùng không?</p>
            </div>
            <div class="modal-footer justify-content-around">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Không</button>
                <a href="#"  class="btn btn-danger btn_confirm_delete_item">Có</a>
            </div>
            </div>
        </div>
    </div>
    <!-- end modal -->
@endsection

@push('js')
<script type="text/javascript" src="{{ cxl_asset('/cms_assets/js/user/index.js') }}"></script>
@endpush
