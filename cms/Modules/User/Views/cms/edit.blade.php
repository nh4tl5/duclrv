@extends('Core::layouts.layout_cms', ['activePage' => 'user', 'titlePage' => __('')])
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('cms.user.update', ['id' => $user->id]) }}" class="form-horizontal">
                        @csrf
                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('ユーザ情報修正') }}</h4>
                                <p class="card-category">{{ __('ユーザの情報') }}</p>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('ユーザ名') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('ユーザ名を入力してください') }}" value="{{ old('name', $user->name) }}" required="true" aria-required="true"/>
                                            @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('メールアドレス') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="{{ __('メールアドレスを入力してください') }}" value="{{ old('email', $user->email) }}" required readonly />
                                            @if ($errors->has('email'))
                                                <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('性別') }}</label>
                                    <div class="col-sm-7 d-flex align-items-end">
                                        <div class="form-check form-check-radio form-check-inline mb-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" {{ old('gender', $user->gender) == 'female' ? 'checked' : '' }} type="radio" name="gender" id="gender1" value="female">女性
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="form-check form-check-radio form-check-inline mb-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" {{ old('gender', $user->gender) == 'male' ? 'checked' : '' }} type="radio" name="gender" id="gender2" value="male">男性
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('役割') }}</label>
                                    <div class="col-sm-7 d-flex align-items-end">
                                        <div class="form-check form-check-radio form-check-inline mb-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" {{ $user->hasRole('admin') || old('role') == 'admin' ? 'checked' : '' }} type="radio" name="role" id="role1" value="admin">Admin
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="form-check form-check-radio form-check-inline mb-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" {{ $user->hasRole('user') || old('role') == 'user' ? 'checked' : '' }} type="radio" name="role" id="role2" value="user">user
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row_user_permission {{ $user->hasRole('admin') || old('role') == 'admin' ? 'd-none' : '' }}">
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('生年月日') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('date_of_birth') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}" name="date_of_birth" id="input-date-of-birth" type="date" placeholder="{{ __('生年月日を選択してください') }}" value="{{ old('date_of_birth', $user->date_of_birth) }}"/>
                                            @if ($errors->has('date_of_birth'))
                                                <span id="date-f-birth-error" class="error text-danger" for="input-date-of-birth">{{ $errors->first('date_of_birth') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label" for="input-addess">{{ __('お住いの都道府県') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" input type="text" name="address" id="input-addess" placeholder="{{ __('場所を入力してください') }}" value="{{ old('address', $user->address) }}" />
                                            @if ($errors->has('address'))
                                                <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('address') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">{{ __('保存') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ cxl_asset('/cms_assets/js/user/save.js') }}"></script>
@endpush
