<?php
//
//namespace Cms\Modules\User\Controllers;
//
//use App\Http\Controllers\Controller;
//use Carbon\Carbon;
//use DateTime;
//use Cms\Modules\User\Requests\CmsUserRequest;
//use Cms\Modules\User\Services\Contracts\UserServiceContract;
//
//class ClientUserController extends Controller
//{
//    protected $service;
//    protected $userCourseService;
//
//    public function __construct(UserServiceContract $service)
//    {
//        $this->service = $service;
//    }
//
//    public function detailUserClient($id)
//    {
//        $user = $this->service->find($id);
//
//        return view('User::client.detail', compact('user');
//    }
//
//    public function updateUserClient($id, CmsUserRequest $request)
//    {
//        $data = [
//            'name' => $request['name'],
//            'address' => $request['address'],
//            'email' => $request['email'],
//            'date_of_birth' => Carbon::createFromFormat('d/m/Y', $request['birthday'])->format('Y-m-d'),
//            'gender' => $request['gender']
//        ];
//        $this->service->update($id, $data);
//
//        return redirect()->route('client.user.detail', ['id' => $id])->with('success', '修正成功しました');
//    }
//}
