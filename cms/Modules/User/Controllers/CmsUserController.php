<?php

namespace Cms\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use Cms\Modules\Test\Services\Contracts\TestTestServiceContract;
use Cms\Modules\Test\Services\Contracts\TestUserTestServiceContract;
use Cms\Modules\User\Requests\CmsUserRequest;
use Cms\Modules\User\Services\Contracts\UserServiceContract;
use Cms\Modules\User\Services\Contracts\UserCourseServiceContract;
use Illuminate\Http\Request;

class CmsUserController extends Controller
{
    protected $service;

    public function __construct(UserServiceContract $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $users = $this->service->getAllPermissionByUser();

        return view('User::cms.index', compact('users'));
    }

    public function detail($id)
    {
        $user = $this->service->find($id);

        return view('User::cms.detail', compact('user'));
    }

    public function create()
    {
        return view('User::cms.create');
    }

    public function store(CmsUserRequest $request)
    {
        $this->service->cmsStore($request->all());

        return redirect()->route('cms.user.create')->with('success', '作成成功しました');
    }

    public function edit($id)
    {
        $user = $this->service->find($id);

        return view('User::cms.edit', compact('user'));
    }

    public function update($id, CmsUserRequest $request)
    {
        $this->service->cmsUpdate($id, $request->all());

        return redirect()->route('cms.user.edit', ['id' => $id])->with('success', '修正成功しました');
    }

    public function delete($id)
    {
        $this->service->delete($id);

        return redirect()->route('cms.user.index')->with('success', '削除しました');
    }

}
