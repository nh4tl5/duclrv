<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'cms',
    'namespace' => 'Cms\Modules\User\Controllers',
    'middleware' => 'web',
], function() {
    Route::group([
        'prefix' => 'user',
        'middleware' => ['auth', 'role:admin']
    ], function () {
        Route::get('/', 'CmsUserController@index')->name('cms.user.index');
        Route::get('/create', 'CmsUserController@create')->name('cms.user.create');
        Route::post('/store', 'CmsUserController@store')->name('cms.user.store');
        Route::get('{id}/edit', 'CmsUserController@edit')->name('cms.user.edit');
        Route::post('/{id}/update', 'CmsUserController@update')->name('cms.user.update');
        Route::get('{id}/delete', 'CmsUserController@delete')->name('cms.user.delete');
        Route::get('{id}/detail', 'CmsUserController@detail')->name('cms.user.detail');
    });

    Route::group([
        'prefix' => 'user-test',
        'middleware' => ['auth', 'role:admin']
    ], function () {
        Route::get('/detail/{id}', 'CmsUserController@testUserDetail')->name('cms.user.test.detail');
        Route::get('/edit/{id}', 'CmsUserController@testUserEdit')->name('cms.user.test.edit');
        Route::get('/delete/{id}', 'CmsUserController@testUserDelete')->name('cms.user.test.delete');
        Route::post('/update/{id}', 'CmsUserController@testUserUpdate')->name('cms.user.test.update');
    });
});

Route::group([
    'namespace' => 'Cms\Modules\User\Controllers',
    'middleware' => ['web', 'auth', 'role:user|admin'],
    'prefix' => 'user',
], function () {
    Route::get('/detail/{id}', 'ClientUserController@detailUserClient')->name('client.user.detail');
    Route::post('/update/{id}', 'ClientUserController@updateUserClient')->name('client.user.update');
});
