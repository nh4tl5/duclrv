<?php

namespace Cms\Modules\User;

use Cms\CmsServiceProvider;
use Cms\Modules\User\Repositories\Contracts\UserRepositoryContract;
use Cms\Modules\User\Repositories\Contracts\UserCourseRepositoryContract;
use Cms\Modules\User\Repositories\UserRepository;
use Cms\Modules\User\Repositories\UserCourseRepository;
use Cms\Modules\User\Services\Contracts\UserServiceContract;
use Cms\Modules\User\Services\Contracts\UserCourseServiceContract;
use Cms\Modules\User\Services\UserService;
use Cms\Modules\User\Services\UserCourseService;
use Illuminate\Routing\Router;

class UserServiceProvider extends CmsServiceProvider
{
    public function boot(Router $router)
    {
        parent::boot($router);
    }

    public function register()
    {
        $this->app->bind(UserServiceContract::class, UserService::class );
        $this->app->bind(UserRepositoryContract::class, UserRepository::class );
        $this->app->bind(UserCourseServiceContract::class, UserCourseService::class);
        $this->app->bind(UserCourseRepositoryContract::class, UserCourseRepository::class);
    }
}
