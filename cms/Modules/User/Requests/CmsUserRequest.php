<?php

namespace Cms\Modules\User\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CmsUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = $this->route('id') ?? null;
        $validates = [
            'name' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users,email,' . $userId],
            'address' => ['max:191'],
            'gender' => ['max:191'],
            'date_of_brth' => ['date'],
        ];

        if (!$userId) {
            $validates['password'] = ['required', 'string', 'min:6', 'confirmed'];
        }

        return $validates;
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}

